use rand::{rngs::ThreadRng, thread_rng, Rng};
use std::collections::HashMap;
use std::fmt;
use yansi::Paint;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Player {
    Blue,
    Yellow,
}

impl Default for Player {
    fn default() -> Self {
        Player::Blue
    }
}

impl Player {
    fn swap(&mut self) {
        match self {
            Player::Blue => *self = Player::Yellow,
            Player::Yellow => *self = Player::Blue,
        }
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Hash)]
struct Board {
    player: Player,
    blues: [u8; 7],
    yellows: [u8; 7],
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut mask = 1u8 << 6;
        for _ in 0..7 {
            for (blue, yellow) in self.blues.iter().zip(self.yellows.iter()) {
                if (blue & mask) == mask {
                    write!(f, "{}", Paint::blue("O").bold())?;
                } else if (yellow & mask) == mask {
                    write!(f, "{}", Paint::yellow("O").bold())?;
                } else {
                    write!(f, " ")?;
                }
            }
            writeln!(f)?;
            mask >>= 1;
        }
        writeln!(f, "{:?} turn", self.player)
    }
}

#[derive(Debug)]
enum Error {
    ColumnFull(usize),
    ColumnBound(usize),
}

impl Board {
    fn add(&mut self, column: usize) -> Result<bool, Error> {
        let (player, opponent) = match self.player {
            Player::Blue => (&mut self.blues, &self.yellows),
            Player::Yellow => (&mut self.yellows, &self.blues),
        };

        let player_col = player.get_mut(column).ok_or(Error::ColumnBound(column))?;
        let opponent_col = opponent.get(column).ok_or(Error::ColumnBound(column))?;

        let row = 8 - (*player_col | *opponent_col).leading_zeros();
        if row == 6 {
            Err(Error::ColumnFull(column))
        } else {
            *player_col |= 1 << row;
            if is_winner(*player, row as usize, column) {
                Ok(true)
            } else {
                self.player.swap();
                Ok(false)
            }
        }
    }

    fn reward(&self, col: usize) -> i32 {
        let mut board = self.clone();

        match board.add(col) {
            Ok(true) => return 100,
            Ok(false) => (),
            Err(_) => return -100,
        }

        let (player, opponent) = match self.player {
            Player::Blue => (&self.blues, &self.yellows),
            Player::Yellow => (&self.yellows, &self.blues),
        };

        let merged = player[col] | opponent[col];
        let row = (8 - merged.leading_zeros()) as usize;

        let reward2s = consecutives(*player, row, col, 2) as i32;
        let reward3s = consecutives(*player, row, col, 3) as i32;

        let penalty2s = consecutives(*opponent, row, col, 2) as i32;
        let penalty3s = consecutives(*opponent, row, col, 3) as i32;

        2 * (reward2s - penalty2s) + 3 * (reward3s - penalty3s)
    }

    fn rewards(&self) -> [f32; 7] {
        let mut rewards = [0.; 7];
        for col in 0..7 {
            rewards[col] = self.reward(col) as f32;
        }
        rewards
    }
}

fn consecutives(board: [u8; 7], row: usize, col: usize, len: usize) -> usize {
    let mut count = 0;

    // check in-column win
    let mask = (1 << len) - 1;
    if row > (6 - len) && (board[col] >> (row + len - 7) & mask) == mask {
        count += 1;
    }

    // check in-rows win
    let row_mask = 1 << row;
    count += board[col.saturating_sub(len - 1)..(col + len).min(7)]
        .windows(len)
        .filter(|w| w.iter().all(|c| c & row_mask == row_mask))
        .count();

    // diagonals
    let mut board_up = board.clone();
    let mut board_down = board.clone();
    for i in 1..7 {
        board_up[i] = board_up[i].rotate_left(i as u32);
        board_down[i] = board_down[i].rotate_right(i as u32);
    }

    let row_mask_up = 1 << (row + col);
    let row_mask_down = 1 << if row >= col { row - col } else { row + 8 - col };
    count += board_up[col.saturating_sub(len - 1)..(col + len).min(7)]
        .windows(len)
        .filter(|w| w.iter().all(|c| c & row_mask_up == row_mask_up))
        .count();

    count += board_down[col.saturating_sub(len - 1)..(col + len).min(7)]
        .windows(len)
        .filter(|w| w.iter().all(|c| c & row_mask_down == row_mask_down))
        .count();

    count
}

fn is_winner(board: [u8; 7], row: usize, col: usize) -> bool {
    consecutives(board, row, col, 4) > 0
}

fn play(
    rng: &mut ThreadRng,
    q: &mut HashMap<Board, [f32; 7]>,
    alpha: f32,
    gamma: f32,
    debug: bool,
) -> Result<(usize, Board), Error> {
    let mut board = Board::default();
    for i in 0..6 * 7 {
        // get entry or create a new one
        let q_entry = q.entry(board.clone()).or_insert_with(|| [0.; 7]);
        let rewards = board.rewards();
        let max = max_q(&*q_entry);
        // update q
        for (q, r) in q_entry.iter_mut().zip(rewards.iter()) {
            *q = (1. - alpha) * *q + alpha * (*r + gamma * max);
        }
        // next step
        let max = max_q(&*q_entry);
        let actions = q_entry
            .iter()
            .enumerate()
            .filter(|(_, q)| **q == max)
            .map(|(i, _)| i)
            .collect::<Vec<_>>();
        let col_sample = actions[rng.gen_range(0, actions.len())];
        if debug {
            println!("{}", board);
        }
        if board.add(col_sample)? {
            if debug {
                println!("{}", board);
                println!("{:?} WON in {} steps", board.player, i + 1);
            }
            return Ok((i, board));
        }
    }
    Ok((6 * 7, board))
}

fn max_q(q_entry: &[f32; 7]) -> f32 {
    let mut max_q = std::f32::NEG_INFINITY;
    for q in q_entry {
        if *q > max_q {
            max_q = *q;
        }
    }
    max_q
}

fn main() -> Result<(), Error> {
    let mut q = HashMap::with_capacity(1000000);
    let mut rng = thread_rng();
    let alpha = 0.3;
    let gamma = 0.4;
    for _ in 0..10_000 {
        play(&mut rng, &mut q, alpha, gamma, false)?;
    }
    play(&mut rng, &mut q, alpha, gamma, true)?;

    Ok(())
}

#[test]
fn in_column() -> Result<(), Error> {
    let mut board = Board::default();
    board.add(0)?;
    board.add(1)?;
    board.add(0)?;
    board.add(1)?;
    board.add(0)?;
    board.add(1)?;
    assert!(board.add(0)?, "{}", board);
    Ok(())
}
